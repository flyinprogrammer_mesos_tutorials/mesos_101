# Setting up consul

## Setup server
The first thing we need to do is setup a consul master.
Login to your first box, and run the following snippets:

```
#!/bin/bash
DATACENTER=dc1

AWS_DNS=$(cat /etc/resolv.conf |grep -i nameserver|head -n1|cut -d ' ' -f2)
BRIDGE_IP=$(docker run --rm alpine sh -c "ip ro get 8.8.8.8 | awk '{print \$3}'")
DOMAIN_NAME=$(curl -s http://169.254.169.254/latest/meta-data/local-hostname | cut -d "." -f 2-)
IP_ADDRESS=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

sudo docker run --restart=always -d --name=dnsmasq --net=host andyshinn/dnsmasq:2.76 -u root --log-facility=- -q -R -h -S $AWS_DNS -S /consul/$BRIDGE_IP#8600
sleep 3
## This will be reset on reboot. Fixing this is not easy.
sudo tee /etc/resolv.conf <<-EOF
search $DOMAIN_NAME
nameserver $BRIDGE_IP
EOF
sleep 10

# Setup a data directory
sudo mkdir -p /consul/{data,config}
sudo chmod 777 -R /consul

# Setup consul
cat <<EOF > /consul/config/00_server.json
{
    "advertise_addr": "$IP_ADDRESS",
    "bootstrap_expect": 1,
    "client_addr": "0.0.0.0",
    "datacenter": "$DATACENTER",
    "disable_remote_exec": true,
    "dns_config": {
        "node_ttl": "10s",
        "allow_stale": true,
        "max_stale": "10s",
        "service_ttl": {
            "*": "10s"
        }
    },
    "log_level": "INFO",
    "leave_on_terminate": true,
    "server": true,
    "ui": true
}
EOF
sudo docker run --restart=always -d --name=consul -v /consul:/consul --net=host consul:v0.7.0 agent
```

## Setup clients

Login to your other boxes and run this:

```
#!/bin/bash
DATACENTER=dc1
CONSUL_SERVER_IP=172.31.8.190

AWS_DNS=$(cat /etc/resolv.conf |grep -i nameserver|head -n1|cut -d ' ' -f2)
BRIDGE_IP=$(docker run --rm alpine sh -c "ip ro get 8.8.8.8 | awk '{print \$3}'")
DOMAIN_NAME=$(curl -s http://169.254.169.254/latest/meta-data/local-hostname | cut -d "." -f 2-)
IP_ADDRESS=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

sudo docker run --restart=always -d --name=dnsmasq --net=host andyshinn/dnsmasq:2.76 -u root --log-facility=- -q -R -h -S $AWS_DNS -S /consul/$BRIDGE_IP#8600
sleep 3
## This will be reset on reboot. Fixing this is not easy.
sudo tee /etc/resolv.conf <<-EOF
search $DOMAIN_NAME
nameserver $BRIDGE_IP
EOF
sleep 10
sudo docker run --restart=always -d --name=consul --net=host consul:v0.7.0 agent -bind=$IP_ADDRESS -client=0.0.0.0 -retry-join=$CONSUL_SERVER_IP -datacenter=$DATACENTER
```